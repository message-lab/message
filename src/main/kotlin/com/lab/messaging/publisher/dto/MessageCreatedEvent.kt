package com.lab.messaging.publisher.dto

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import java.util.UUID

@JsonIgnoreProperties(ignoreUnknown = true)
data class MessageCreatedEvent(
    val id: UUID = UUID.randomUUID(),
    val from: UUID,
    val to: UUID,
    val content: String
)
