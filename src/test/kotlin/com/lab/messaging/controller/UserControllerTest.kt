package com.lab.messaging.controller

import com.lab.messaging.domain.dto.request.CreateUserRequest
import com.lab.messaging.domain.dto.response.CreateUserResponse
import com.lab.messaging.exception.UserAlreadyExistsException
import com.lab.messaging.objectMapper
import com.lab.messaging.service.UserService
import com.ninjasquad.springmockk.MockkBean
import io.mockk.clearAllMocks
import io.mockk.every
import org.junit.jupiter.api.AfterEach
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import java.util.UUID
import kotlin.test.Test

@WebMvcTest(controllers = [UserController::class])
internal class UserControllerTest {

    @Autowired
    private lateinit var mockMvc: MockMvc

    @MockkBean
    private lateinit var userService: UserService

    @AfterEach
    fun clean() {
        clearAllMocks()
    }

    @Test
    fun `should create message with 201 CREATED Response with created user's id`() {
        val createUserRequest = CreateUserRequest(nickname = "nickname")

        every { userService.create(any()) } returns CreateUserResponse(UUID.randomUUID())

        val result = mockMvc.perform(
            MockMvcRequestBuilders.post("/api/users").contentType(MediaType.APPLICATION_JSON)
                .header("userId", UUID.randomUUID().toString())
                .content(objectMapper.writeValueAsString(createUserRequest))
        )

        result.andExpect(MockMvcResultMatchers.status().isCreated)
            .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
    }

    @Test
    fun `should return message with 409 CONFLICT Response if nickname already exists`() {
        val createUserRequest = CreateUserRequest(nickname = "nickname")

        every { userService.create(any()) } throws UserAlreadyExistsException("")

        val result = mockMvc.perform(
            MockMvcRequestBuilders.post("/api/users").contentType(MediaType.APPLICATION_JSON)
                .header("userId", UUID.randomUUID().toString())
                .content(objectMapper.writeValueAsString(createUserRequest))
        )

        result.andExpect(MockMvcResultMatchers.status().isConflict).andExpect(
            MockMvcResultMatchers.jsonPath("$.errorMessage").value("")
        )
    }
}
