package com.lab.messaging.publisher

import com.lab.messaging.PublishEvents
import com.lab.messaging.SNS
import com.lab.messaging.publisher.configuration.SnsPublisher
import com.lab.messaging.publisher.dto.MessageCreatedEvent
import io.awspring.cloud.sns.core.SnsTemplate
import mu.KotlinLogging
import org.springframework.stereotype.Component

@Component
class MessagePublisher(snsTemplate: SnsTemplate) : SnsPublisher(snsTemplate) {
    private val logger = KotlinLogging.logger {}

    fun publishMessageCreatedEvent(messageCreatedEvent: MessageCreatedEvent) {
        sendEvent(
            eventClass = PublishEvents.MESSAGE_EVENT,
            payload = messageCreatedEvent,
            topicName = SNS.MESSAGE_TOPIC
        )
        logger.info { "${PublishEvents.MESSAGE_EVENT} with from : ${messageCreatedEvent.from}, to: ${messageCreatedEvent.to} has been sent to '${SNS.MESSAGE_TOPIC}'" }
    }
}
