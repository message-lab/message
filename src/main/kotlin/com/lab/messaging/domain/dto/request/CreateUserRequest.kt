package com.lab.messaging.domain.dto.request

data class CreateUserRequest(
    val nickname: String
)
