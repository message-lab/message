CREATE TABLE "user" (
  id                        uuid                                NOT NULL DEFAULT gen_random_uuid(),
  nickname                  character varying(255)              NOT NULL UNIQUE,
  CONSTRAINT user_pkey PRIMARY KEY (id)
);