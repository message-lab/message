package com.lab.messaging.controller

import com.lab.messaging.domain.dto.request.CreateUserRequest
import com.lab.messaging.domain.dto.response.CreateUserResponse
import com.lab.messaging.service.UserService
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/" + "users")
class UserController(
    private val userService: UserService
) {

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun createUser(
        @RequestBody createUserRequest: CreateUserRequest
    ): CreateUserResponse {
        return userService.create(createUserRequest)
    }
}
