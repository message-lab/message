# Messaging Application
this application can be used to send messages between clients
there is no authentication/authorization
as long as userId is present on the header, and that userId exists it can be used to impersonate any user 

### Features
- as a non-user, I should be able to create an account if nickname is not already taken
- as a user, I should be able to send message to another user
- as a user, I should be able to see all my received messages
- as a user, I should be able to see all my received messages from particular user
- as a user, I should be able to see all my sent messages

all the listed features integration tests can be found at test/MessageIntegrationTest.kt

### How To Run For Local Testing
simply calling bootTestRun command of gradle will make the app running locally with dockerized components

```
gradle bootTestRun
```

after this, the application should run at 8080 port.

navigating to `localhost:8080/swagger-ui.html` the rest api endpoints can be seen and interact with

all the resources will be run inside testcontainers, which are:

- Localstack (for AWS SQS queue and SNS topic)
- Postgres (for the database)

note that: all the aws infrastructure is mocked in test/resources/init-aws.sh to pass into localstack context

### Overall Diagram

![overall-diagram.png](overall-diagram.png)

# Roadmap (To be improved)

- introduce authentication mechanism with JWT (Keycloak can be used here)
- introduce pagination to fetch messages (if there are too many messages for a particular user, it wouldn't be good to fetch it all)