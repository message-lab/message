package com.lab.messaging

import com.lab.messaging.domain.dto.request.CreateMessageRequest
import com.lab.messaging.domain.dto.request.CreateUserRequest
import com.lab.messaging.domain.dto.request.enum.MessageType
import com.lab.messaging.repository.MessageRepository
import com.lab.messaging.repository.UserRepository
import com.lab.messaging.service.MessageService
import com.lab.messaging.service.TestUtils.message
import com.lab.messaging.service.TestUtils.user
import com.lab.messaging.service.UserService
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import kotlin.test.assertEquals

@SpringBootTest
internal class MessageIntegrationTest : AbstractTestcontainersIntegrationTest() {

    @Autowired
    private lateinit var messageService: MessageService

    @Autowired
    private lateinit var userService: UserService

    @Autowired
    private lateinit var messageRepository: MessageRepository

    @Autowired
    private lateinit var userRepository: UserRepository

    @AfterEach
    fun cleanUp() {
        userRepository.deleteAll()
        messageRepository.deleteAll()
    }

    @Test
    fun `as a non-user, I should be able to create an account if nickname is not already taken`() {
        // given
        val createdUserResponse = userService.create(CreateUserRequest("nickname"))

        // when
        val result = userRepository.fetchByNickname("nickname")

        // then
        assertEquals(createdUserResponse.id, result?.id)
    }

    @Test
    fun `as a user, I should be able to send message to another user`() {
        // given
        val sender = userRepository.save(user(nickname = "sender"))
        val receiver = userRepository.save(user(nickname = "receiver"))
        val message = messageRepository.save(message(to = receiver.id!!, from = sender.id!!))

        // when
        val result =
            messageService.fetchAllMessagesOfUserByMessageType(userId = receiver.id!!.toString(), type = MessageType.RECEIVED)

        // then
        assertEquals(message.to, result.first().to)
        assertEquals(message.from, result.first().from)
        assertEquals(message.id, result.first().id)
        assertEquals(message.content, result.first().content)
    }

    @Test
    fun `as a user, I should be able to see all my received messages`() {
        // given
        val sender = userRepository.save(user(nickname = "sender"))
        val receiver = userRepository.save(user(nickname = "receiver"))

        // when

        val message = messageService.create(senderId = sender.id!!.toString(), createMessageRequest = CreateMessageRequest(to = receiver.id.toString(), content = ""))

        val result =
            messageService.fetchAllMessagesOfUserByMessageType(userId = receiver.id!!.toString(), type = MessageType.RECEIVED)

        // then
        assertEquals(message.to, result.first().to)
        assertEquals(message.from, result.first().from)
        assertEquals(message.id, result.first().id)
        assertEquals(message.content, result.first().content)
    }

    @Test
    fun `as a user, I should be able to see all my received messages from particular user`() {
        // given
        val sender = userRepository.save(user(nickname = "sender"))
        val receiver = userRepository.save(user(nickname = "receiver"))

        val message = messageService.create(senderId = sender.id!!.toString(), createMessageRequest = CreateMessageRequest(to = receiver.id.toString(), content = ""))

        // when
        val result = messageService.fetchAllMessagesOfUserByMessageType(
            userId = receiver.id!!.toString(),
            type = MessageType.RECEIVED,
            from = sender.id!!.toString()
        )

        // then
        assertEquals(message.to, result.first().to)
        assertEquals(message.from, result.first().from)
        assertEquals(message.id, result.first().id)
        assertEquals(message.content, result.first().content)
    }

    @Test
    fun `as a user, I should be able to see all my sent messages`() {
        // given
        val sender = userRepository.save(user(nickname = "sender"))
        val receiver = userRepository.save(user(nickname = "receiver"))

        val message = messageService.create(senderId = sender.id!!.toString(), createMessageRequest = CreateMessageRequest(to = receiver.id.toString(), content = ""))

        // when
        val result = messageService.fetchAllMessagesOfUserByMessageType(userId = sender.id!!.toString(), type = MessageType.SENT)

        // then
        assertEquals(message.to, result.first().to)
        assertEquals(message.from, result.first().from)
        assertEquals(message.id, result.first().id)
        assertEquals(message.content, result.first().content)
    }
}
