package com.lab.messaging.service

import com.lab.messaging.domain.dto.request.CreateMessageRequest
import com.lab.messaging.domain.dto.request.enum.MessageType
import com.lab.messaging.domain.model.Message
import com.lab.messaging.publisher.MessagePublisher
import com.lab.messaging.publisher.dto.MessageCreatedEvent
import com.lab.messaging.repository.MessageRepository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.UUID

@Service
class MessageService(private val messageRepository: MessageRepository, private val messagePublisher: MessagePublisher) {

    @Transactional
    fun create(senderId: String, createMessageRequest: CreateMessageRequest): Message {
        if (senderId == createMessageRequest.to) {
            throw IllegalStateException("sender cannot send message to itself")
        }

        return messageRepository.save(
            Message(
                from = UUID.fromString(senderId),
                to = UUID.fromString(createMessageRequest.to),
                content = createMessageRequest.content
            )
        ).also {
            messagePublisher.publishMessageCreatedEvent(
                MessageCreatedEvent(
                    from = it.from,
                    to = it.to,
                    content = it.content
                )
            )
        }
    }

    fun fetchAllMessagesOfUserByMessageType(userId: String, type: MessageType, from: String? = null): List<Message> {
        return when (type) {
            MessageType.RECEIVED -> {
                if (from != null) {
                    messageRepository.fetchAllByToAndFrom(to = UUID.fromString(userId), from = UUID.fromString(from))
                } else {
                    messageRepository.fetchAllByTo(to = UUID.fromString(userId))
                }
            }

            MessageType.SENT -> {
                messageRepository.fetchAllByFrom(from = UUID.fromString(userId))
            }
        }
    }
}
