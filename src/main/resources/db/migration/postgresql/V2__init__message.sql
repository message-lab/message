CREATE TABLE message (
  id                        uuid                                NOT NULL DEFAULT gen_random_uuid(),
  "from"                    uuid                                NOT NULL,
  "to"                      uuid                                NOT NULL,
  content                  character varying(255)               NOT NULL,
  CONSTRAINT message_pkey PRIMARY KEY (id)
);