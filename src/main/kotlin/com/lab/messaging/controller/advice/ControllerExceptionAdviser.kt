package com.lab.messaging.controller.advice

import com.lab.messaging.controller.MessageController
import com.lab.messaging.controller.UserController
import com.lab.messaging.domain.dto.response.ErrorResponse
import com.lab.messaging.exception.UserAlreadyExistsException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import java.time.Instant

@ControllerAdvice(
    basePackageClasses = [
        UserController::class,
        MessageController::class
    ]
)
class ControllerExceptionAdviser : ResponseEntityExceptionHandler() {

    @ExceptionHandler(
        value = [
            IllegalStateException::class
        ]
    )
    fun handleIllegalStateException(
        exception: IllegalStateException,
        request: WebRequest
    ): ResponseEntity<ErrorResponse> {
        return ResponseEntity(
            ErrorResponse(
                Instant.now(),
                errorMessage = exception.message
            ),
            HttpStatus.BAD_REQUEST
        )
    }

    @ExceptionHandler(
        value = [
            UserAlreadyExistsException::class
        ]
    )
    fun handleUserAlreadyExistsException(
        exception: UserAlreadyExistsException,
        request: WebRequest
    ): ResponseEntity<ErrorResponse> {
        return ResponseEntity(
            ErrorResponse(
                Instant.now(),
                errorMessage = exception.message
            ),
            HttpStatus.CONFLICT
        )
    }
}
