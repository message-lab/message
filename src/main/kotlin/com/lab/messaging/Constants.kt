package com.lab.messaging

object PublishEvents {
    const val MESSAGE_EVENT = "MessageCreatedEvent"
}

object SNS {
    const val MESSAGE_TOPIC = "messages-topic"
}
