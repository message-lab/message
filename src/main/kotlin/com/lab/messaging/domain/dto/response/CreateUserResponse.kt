package com.lab.messaging.domain.dto.response

import java.util.UUID

data class CreateUserResponse(
    val id: UUID
)
