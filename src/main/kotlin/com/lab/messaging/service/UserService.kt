package com.lab.messaging.service

import com.lab.messaging.domain.dto.request.CreateUserRequest
import com.lab.messaging.domain.dto.response.CreateUserResponse
import com.lab.messaging.domain.model.User
import com.lab.messaging.exception.UserAlreadyExistsException
import com.lab.messaging.repository.UserRepository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class UserService(private val userRepository: UserRepository) {

    @Transactional
    fun create(createUserRequest: CreateUserRequest): CreateUserResponse {
        if (userRepository.fetchByNickname(createUserRequest.nickname) != null) {
            throw UserAlreadyExistsException("cannot create user with nickname: ${createUserRequest.nickname}, already being used")
        }

        return with(
            userRepository.save(
                User(
                    nickname = createUserRequest.nickname
                )
            )
        ) {
            CreateUserResponse(id = this.id!!)
        }
    }
}
