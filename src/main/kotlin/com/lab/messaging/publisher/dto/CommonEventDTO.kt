package com.lab.messaging.publisher.dto

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
data class CommonEventDTO<T : Any>(
    val eventPayload: T,
    val eventClass: String
)
