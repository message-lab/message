#!/usr/bin/env bash

set -euo pipefail

LOCALSTACK_DEFAULT_AWS_ACCOUNT_ID=000000000000
REGION=us-east-1

########### SQS ###########
echo "########### Creating messages-queue ... ###########"
MESSAGES_QUEUE_NAME=messages-queue
awslocal sqs create-queue --queue-name $MESSAGES_QUEUE_NAME --region $REGION --output table
MESSAGES_QUEUE_ARN=arn:aws:sqs:$REGION:$LOCALSTACK_DEFAULT_AWS_ACCOUNT_ID:$MESSAGES_QUEUE_NAME

########### SNS ###########
echo "########### Creating messages-topic... ###########"
MESSAGES_TOPIC_NAME=messages-topic
awslocal sns create-topic --name $MESSAGES_TOPIC_NAME --region $REGION --output table
MESSAGES_TOPIC_ARN=arn:aws:sns:$REGION:$LOCALSTACK_DEFAULT_AWS_ACCOUNT_ID:$MESSAGES_TOPIC_NAME

########### SNS to SQS subscription ###########
echo "########### Create subscription from [messages-topic] to [messages-queue] ... ###########"
awslocal sns subscribe --region $REGION \
  --topic-arn $MESSAGES_TOPIC_ARN \
  --protocol sqs \
  --notification-endpoint $MESSAGES_QUEUE_ARN \
  --attributes RawMessageDelivery=true \
  --output table

echo "Resources initialized."