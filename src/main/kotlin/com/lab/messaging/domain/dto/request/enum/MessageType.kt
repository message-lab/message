package com.lab.messaging.domain.dto.request.enum

enum class MessageType {
    RECEIVED,
    SENT
}
