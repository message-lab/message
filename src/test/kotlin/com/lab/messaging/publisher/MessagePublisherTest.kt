package com.lab.messaging.publisher

import com.lab.messaging.PublishEvents.MESSAGE_EVENT
import com.lab.messaging.SNS
import com.lab.messaging.publisher.dto.CommonEventDTO
import com.lab.messaging.publisher.dto.MessageCreatedEvent
import io.awspring.cloud.sns.core.SnsTemplate
import io.mockk.clearAllMocks
import io.mockk.justRun
import io.mockk.mockk
import io.mockk.slot
import io.mockk.verify
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.springframework.messaging.Message
import java.util.UUID
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class MessagePublisherTest {
    private val snsTemplate = mockk<SnsTemplate>()
    private val messagePublisher = MessagePublisher(snsTemplate)

    @AfterEach
    fun clean() {
        clearAllMocks()
    }

    @Test
    fun `send messageCreatedEvent`() {
        // given
        val messageCreatedEvent = MessageCreatedEvent(
            from = UUID.randomUUID(),
            to = UUID.randomUUID(),
            content = "content"
        )
        val messageSlot = slot<Message<CommonEventDTO<MessageCreatedEvent>>>()
        val headersSlot = slot<Map<String, Any>>()

        justRun {
            snsTemplate.convertAndSend(
                SNS.MESSAGE_TOPIC,
                capture(messageSlot),
                capture(headersSlot)
            )
        }

        // when
        messagePublisher.publishMessageCreatedEvent(messageCreatedEvent)

        // then
        verify(exactly = 1) {
            snsTemplate.convertAndSend(
                SNS.MESSAGE_TOPIC,
                any<MessageCreatedEvent>(),
                any<Map<String, Any>>()
            )
        }

        val message = messageSlot.captured
        val messageCreatedEventDTO = message.payload
        assertEquals(MESSAGE_EVENT, messageCreatedEventDTO.eventClass)
        assertEquals(messageCreatedEvent, messageCreatedEventDTO.eventPayload)

        val headers = headersSlot.captured
        assertNotNull(headers["eventClass"])
        assertEquals(MESSAGE_EVENT, headers["eventClass"])
    }
}
