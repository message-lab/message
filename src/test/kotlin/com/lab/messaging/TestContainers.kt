package com.lab.messaging

import org.testcontainers.containers.PostgreSQLContainer
import org.testcontainers.containers.localstack.LocalStackContainer
import org.testcontainers.containers.wait.strategy.Wait
import org.testcontainers.utility.DockerImageName
import org.testcontainers.utility.MountableFile

private const val LOCALSTACK_IMAGE = "localstack/localstack:2.3.0"
private const val POSTGRES_IMAGE = "postgres:13.3"

object LocalStackContainer {
    val instance by lazy {
        startLocalStackContainer().also { localstack ->
            System.setProperty("spring.cloud.aws.region.static", localstack.region)
            System.setProperty("spring.cloud.aws.credentials.access-key", localstack.accessKey)
            System.setProperty("spring.cloud.aws.credentials.secret-key", localstack.secretKey)
            System.setProperty(
                "spring.cloud.aws.sqs.endpoint",
                localstack.getEndpointOverride(LocalStackContainer.Service.SQS).toString()
            )
            System.setProperty(
                "spring.cloud.aws.sns.endpoint",
                localstack.getEndpointOverride(LocalStackContainer.Service.SNS).toString()
            )
        }
    }

    private fun startLocalStackContainer() = LocalStackContainer(DockerImageName.parse(LOCALSTACK_IMAGE)).apply {
        withCopyToContainer(
            MountableFile.forClasspathResource("/localstack/init-aws.sh", 755),
            "/etc/localstack/init/ready.d/init-aws.sh"
        )
        withCommand("chmod u+x /etc/localstack/init/ready.d/init-aws.sh")
        withServices(
            LocalStackContainer.Service.SQS,
            LocalStackContainer.Service.SNS
        )
        waitingFor(Wait.forLogMessage(".*Resources initialized\\.\\n", 1))
        start()
    }
}

object PostgresContainer {
    val instance by lazy {
        startPostgresContainer().also { postgresContainer ->
            System.setProperty(
                "spring.datasource.url",
                "jdbc:postgresql://${postgresContainer.host}:${postgresContainer.getMappedPort(PostgreSQLContainer.POSTGRESQL_PORT)}/${postgresContainer.databaseName}?stringtype=unspecified"
            )
            System.setProperty("spring.datasource.username", postgresContainer.username)
            System.setProperty("spring.datasource.password", postgresContainer.password)
        }
    }

    private fun startPostgresContainer() = PostgreSQLContainer(DockerImageName.parse(POSTGRES_IMAGE)).apply {
        withUsername("postgres")
        withPassword("postgres")
        withDatabaseName("testDb")
        withExposedPorts(5432)
        start()
    }
}
