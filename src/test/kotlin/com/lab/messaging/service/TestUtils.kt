package com.lab.messaging.service

import com.lab.messaging.domain.model.Message
import com.lab.messaging.domain.model.User
import java.util.UUID

object TestUtils {

    fun user(
        id: UUID? = null,
        nickname: String? = null
    ): User = User(
        id = id,
        nickname = nickname ?: "nickname"
    )

    fun message(
        id: UUID? = null,
        from: UUID = UUID.randomUUID(),
        to: UUID = UUID.randomUUID(),
        message: String? = null
    ): Message = Message(
        id = id,
        from = from,
        to = to,
        content = message ?: ""
    )
}
