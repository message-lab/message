package com.lab.messaging

import org.springframework.boot.fromApplication
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.boot.with
import org.springframework.context.annotation.Bean

@TestConfiguration(proxyBeanMethods = false)
class TestAppConfig {
    @Bean
    fun localStack() = LocalStackContainer.instance

    @Bean
    fun postgres() = PostgresContainer.instance
}

fun main(args: Array<String>) {
    fromApplication<MessagingApplication>()
        .with(TestAppConfig::class)
        .run(*args)
}
