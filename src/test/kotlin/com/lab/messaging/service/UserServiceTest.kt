package com.lab.messaging.service

import com.lab.messaging.domain.dto.request.CreateUserRequest
import com.lab.messaging.exception.UserAlreadyExistsException
import com.lab.messaging.repository.UserRepository
import com.lab.messaging.service.TestUtils.user
import io.mockk.clearAllMocks
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.util.UUID
import kotlin.test.AfterTest
import kotlin.test.assertEquals

class UserServiceTest {
    private val userRepository = mockk<UserRepository>()
    private val sut = UserService(
        userRepository = userRepository
    )

    @AfterTest
    fun clean() {
        clearAllMocks()
    }

    @Test
    fun `should create user`() {
        // given
        val user = user(id = UUID.randomUUID())

        every { userRepository.fetchByNickname(user.nickname) } returns null
        every { userRepository.save(any()) } returns user

        // when
        val result = sut.create(CreateUserRequest(nickname = "nickname"))

        // then
        assertEquals(user.id, result.id)
    }

    @Test
    fun `should not create user if nickname exists`() {
        // given
        val user = user(id = UUID.randomUUID())

        every { userRepository.fetchByNickname(user.nickname) } returns user
        every { userRepository.save(any()) } returns user

        // when
        assertThrows<UserAlreadyExistsException> { sut.create(CreateUserRequest(nickname = "nickname")) }

        // then
        verify(exactly = 1) {
            userRepository.fetchByNickname(any())
        }

        verify(exactly = 0) {
            userRepository.save(any())
        }
    }
}
