package com.lab.messaging.domain.dto.request

data class CreateMessageRequest(
    val to: String,
    val content: String
)
