package com.lab.messaging.repository

import com.lab.messaging.domain.model.User
import org.springframework.data.jdbc.repository.query.Query
import org.springframework.data.repository.ListCrudRepository
import org.springframework.stereotype.Repository
import java.util.UUID

@Repository
interface UserRepository : ListCrudRepository<User, UUID> {
    @Query(
        """
        SELECT * FROM "user" WHERE nickname=:nickname
        """
    )
    fun fetchByNickname(nickname: String): User?
}
