package com.lab.messaging.domain.model

import org.springframework.data.annotation.Id
import org.springframework.data.annotation.PersistenceCreator
import org.springframework.data.relational.core.mapping.Table
import java.util.UUID

@Table
data class User @PersistenceCreator constructor(
    @Id val id: UUID? = null,
    val nickname: String
)
