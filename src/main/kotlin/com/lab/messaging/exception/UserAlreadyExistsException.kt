package com.lab.messaging.exception

class UserAlreadyExistsException(message: String?) : RuntimeException(message)
