package com.lab.messaging.service

import com.lab.messaging.domain.dto.request.CreateMessageRequest
import com.lab.messaging.publisher.MessagePublisher
import com.lab.messaging.repository.MessageRepository
import io.mockk.Called
import io.mockk.Runs
import io.mockk.clearAllMocks
import io.mockk.every
import io.mockk.just
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.util.UUID
import kotlin.test.AfterTest
import kotlin.test.assertEquals

class MessageServiceTest {
    private val messageRepository = mockk<MessageRepository>()
    private val messagePublisher = mockk<MessagePublisher>()
    private val sut = MessageService(
        messageRepository = messageRepository,
        messagePublisher = messagePublisher
    )

    @AfterTest
    fun clean() {
        clearAllMocks()
    }

    @Test
    fun `should create message`() {
        // given
        val senderId = UUID.randomUUID()

        val message = TestUtils.message(id = UUID.randomUUID(), from = senderId)

        every { messageRepository.save(any()) } returns message
        every { messagePublisher.publishMessageCreatedEvent(any()) } just Runs

        // when
        val result = sut.create(
            senderId = senderId.toString(),
            CreateMessageRequest(to = message.to.toString(), content = "content")
        )

        // then
        assertEquals(message.id, result.id)
        assertEquals(message.to, result.to)
        assertEquals(senderId, result.from)
        assertEquals(message.content, result.content)
    }

    @Test
    fun `should not create message if sender and receiver are the same`() {
        // given
        val senderId = UUID.randomUUID()

        val message = TestUtils.message(id = UUID.randomUUID(), from = senderId, to = senderId)

        every { messageRepository.save(any()) } returns message
        every { messagePublisher.publishMessageCreatedEvent(any()) } just Runs

        // when
        assertThrows<IllegalStateException> {
            sut.create(
                senderId = senderId.toString(),
                CreateMessageRequest(to = message.to.toString(), content = "content")
            )
        }

        // then
        verify {
            messageRepository wasNot Called
            messagePublisher wasNot Called
        }
    }
}
