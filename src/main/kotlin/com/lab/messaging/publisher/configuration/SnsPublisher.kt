package com.lab.messaging.publisher.configuration

import com.lab.messaging.publisher.dto.CommonEventDTO
import io.awspring.cloud.sns.core.SnsTemplate
import mu.KotlinLogging
import org.springframework.messaging.support.MessageBuilder

open class SnsPublisher(private val snsTemplate: SnsTemplate) {
    private val logger = KotlinLogging.logger {}

    fun sendEvent(eventClass: String, payload: Any, topicName: String) {
        logger.trace { "Event: $payload" }
        val msg = MessageBuilder.withPayload(
            CommonEventDTO(
                eventClass = eventClass,
                eventPayload = payload
            )
        ).build()

        snsTemplate.convertAndSend(topicName, msg, mapOf(Pair("eventClass", eventClass)))
    }

    fun sendEvent(payload: Any, topicName: String) {
        logger.trace { "Event: $payload" }
        snsTemplate.convertAndSend(topicName, payload)
    }
}
