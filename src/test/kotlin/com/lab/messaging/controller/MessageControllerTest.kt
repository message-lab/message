package com.lab.messaging.controller

import com.lab.messaging.domain.dto.request.CreateMessageRequest
import com.lab.messaging.objectMapper
import com.lab.messaging.service.MessageService
import com.lab.messaging.service.TestUtils.message
import com.ninjasquad.springmockk.MockkBean
import io.mockk.clearAllMocks
import io.mockk.every
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import java.util.UUID

@WebMvcTest(controllers = [MessageController::class])
internal class MessageControllerTest {

    @Autowired
    private lateinit var mockMvc: MockMvc

    @MockkBean
    private lateinit var messageService: MessageService

    @AfterEach
    fun clean() {
        clearAllMocks()
    }

    @Test
    fun `should create message with 204 NO_CONTENT Response`() {
        val createMessageRequest = CreateMessageRequest(to = UUID.randomUUID().toString(), content = "")

        every { messageService.create(any(), any()) } returns message()

        val result = mockMvc.perform(
            post("/api/messages").contentType(MediaType.APPLICATION_JSON)
                .header("userId", UUID.randomUUID().toString())
                .content(objectMapper.writeValueAsString(createMessageRequest))
        )

        result.andExpect(MockMvcResultMatchers.status().isNoContent)
    }

    @Test
    fun `should return 400 BAD_REQUEST response if there is no userId in the header`() {
        val createMessageRequest = CreateMessageRequest(to = UUID.randomUUID().toString(), content = "")

        every { messageService.create(any(), any()) } returns message()

        val result = mockMvc.perform(
            post("/api/messages").contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createMessageRequest))
        )

        result.andExpect(MockMvcResultMatchers.status().isBadRequest)
            .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_PROBLEM_JSON))
    }
}
