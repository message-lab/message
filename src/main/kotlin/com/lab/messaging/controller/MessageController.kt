package com.lab.messaging.controller

import com.lab.messaging.domain.dto.request.CreateMessageRequest
import com.lab.messaging.domain.dto.request.enum.MessageType
import com.lab.messaging.domain.model.Message
import com.lab.messaging.service.MessageService
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/" + "messages")
class MessageController(
    private val messageService: MessageService
) {

    @PostMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun createMessage(
        @RequestHeader userId: String,
        @RequestBody createMessageRequest: CreateMessageRequest
    ) {
        messageService.create(senderId = userId, createMessageRequest = createMessageRequest)
    }

    @GetMapping
    fun getMessages(
        @RequestHeader userId: String,
        @RequestParam type: MessageType,
        @RequestParam from: String? = null
    ): List<Message> {
        return messageService.fetchAllMessagesOfUserByMessageType(userId = userId, type = type, from = from)
    }
}
