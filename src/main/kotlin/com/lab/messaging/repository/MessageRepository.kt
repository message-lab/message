package com.lab.messaging.repository

import com.lab.messaging.domain.model.Message
import org.springframework.data.jdbc.repository.query.Query
import org.springframework.data.repository.ListCrudRepository
import org.springframework.stereotype.Repository
import java.util.UUID

@Repository
interface MessageRepository : ListCrudRepository<Message, UUID> {
    @Query(
        """
        SELECT * FROM message WHERE "to"=:to
        """
    )
    fun fetchAllByTo(to: UUID): List<Message>

    @Query(
        """
        SELECT * FROM message WHERE "from"=:from
        """
    )
    fun fetchAllByFrom(from: UUID): List<Message>

    @Query(
        """
        SELECT * FROM message WHERE "to"=:to AND "from"=:from
        """
    )
    fun fetchAllByToAndFrom(from: UUID, to: UUID): List<Message>
}
